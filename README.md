# remoteuserssl-simplesamlphp-module

![maintenance status: unmaintained](https://img.shields.io/maintenance/end%20of%20life/2021)

This project is not maintained anymore, which means it does not receive any Security patches or bug fixes. Check out [SATOSA](https://github.com/IdentityPython/SATOSA) instead.

## Description

SimpleSAMLphp module for getting user identity from REMOTE_USER or SSL_CLIENT_S_CERT variables

See [docs/RemoteUserSSL.md](./docs/RemoteUserSSL.md) for more information.
